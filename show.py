# auothor:给阿龙摘星星
# qq：3157629405
# 声明：本程序仅作学习交流使用，切勿用于商业用途

from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.edge.options import Options
from selenium.webdriver.chrome.options import Options
from ttkbootstrap.dialogs.dialogs import Messagebox
import os
import re
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from ttkbootstrap.scrolled import ScrolledText
import threading


class show_pachong(ttk.Frame):

    def __init__(self, master):
        super().__init__(master, padding=(20, 10))
        self.pack(fill=BOTH, expand=YES)

        self.frame_l = ttk.Frame(self, padding=[10, 5, 10, 5], height=1000, width=600)
        self.frame_m = ttk.Frame(self, padding=[10, 5, 10, 5], height=1000, width=600)
        self.frame_r = ttk.Frame(self, padding=[10, 5, 10, 5], height=1000, width=600)
        self.frame_l.place(x=0, y=0)
        self.frame_m.place(x=600, y=0)
        self.frame_r.place(x=1200, y=0)

        self.usercode = None
        self.password = None
        self.find_data = None
        self.num = 0
        self.break_time = None

        self.usercode_flag = 0

        self.root_dir = os.path.abspath(os.curdir)

        def check_div(str_div) -> bool:
            if re.match(r"\d+", str_div):
                self.st.insert(INSERT, "时间间隔输入正确" + str_div + '\n')
                self.break_time = int(str_div)
                return True
            else:
                Messagebox.show_error("同学您输入的时间间隔不正确", title='温馨提示', parent=None, alert=True)
                self.st.insert(INSERT, "时间间隔输入错误\n")
                return False

        def check_tittle(str_tittle) -> bool:
            self.st.insert(INSERT, "主题已输入" + str_tittle + '\n')
            self.find_data = str_tittle
            return True

        def check_mima(str_mima) -> bool:
            self.st.insert(INSERT, "密码已输入" + '\n')
            self.password = str_mima
            return True

        def check_id(str_id) -> bool:
            if re.match(r"\d{10}", str_id):
                self.st.insert(INSERT, "学号输入正确" + str_id + '\n')
                self.usercode = str_id
                self.usercode_flag = 1
                return True
            else:
                Messagebox.show_error("同学您输入的学号不正确", title='温馨提示', parent=None, alert=True)
                self.st.insert(INSERT, "学号输入错误" + '\n')
                self.usercode_flag = 0
                return False

        def check_num(str_num) -> bool:
            if re.match(r"\d+", str_num):
                self.st.insert(INSERT, "数量输入正确" + str_num + '\n')
                self.num = int(str_num)
                return True
            else:
                Messagebox.show_error("同学您输入的数量不正确", title='温馨提示', parent=None, alert=True)
                self.st.insert(INSERT, "数量输入错误" + '\n')
                return False

        check_id_func = self.register(check_id)
        check_num_func = self.register(check_num)
        check_mima_func = self.register(check_mima)
        check_tittle_func = self.register(check_tittle)
        check_div_func = self.register(check_div)

        # validate numeric entry
        ttk.Label(self.frame_l, text="学号", font=("华文新魏", 20, "bold"), background=None).grid(row=1, column=1,
                                                                                                  padx='21px',
                                                                                                  ipady="15px")
        self.id_entry = ttk.Entry(self.frame_l, validate="focusout", validatecommand=(check_id_func, '%P'))
        self.id_entry.grid(row=1, column=2)
        ttk.Label(self.frame_l, text="(默认学号)", font=("华文行楷", 10, "italic"), style="danger").grid(row=1,
                                                                                                         column=3,
                                                                                                         sticky="W",
                                                                                                         padx='21px',
                                                                                                         ipady="15px")

        # validate alpha entry
        ttk.Label(self.frame_l, text="密码", font=("华文新魏", 20, "bold")).grid(row=2, column=1, padx='21px',
                                                                                 ipady="15px")
        self.mima_entry = ttk.Entry(self.frame_l, validate="focusout", show="*",
                                    validatecommand=(check_mima_func, '%P'))
        self.mima_entry.grid(row=2, column=2)
        ttk.Label(self.frame_l, text="(默认身份证\n后8位)", font=("华文行楷", 10, "italic"), style="danger").grid(row=2,
                                                                                                                  column=3,
                                                                                                                  sticky="W",
                                                                                                                  padx='21px',
                                                                                                                  ipady="15px")

        # validate numeric entry
        ttk.Label(self.frame_l, text="主题", font=("华文新魏", 20, "bold")).grid(row=3, column=1, padx='21px',
                                                                                 ipady="15px")
        self.tittle_entry = ttk.Entry(self.frame_l, validate="focusout", validatecommand=(check_tittle_func, '%P'))
        self.tittle_entry.grid(row=3, column=2)
        ttk.Label(self.frame_l, text="(要下载论文\n的主题)", font=("华文行楷", 10, "italic"), style="danger").grid(
            row=3,
            column=3,
            sticky="W",
            padx='21px',
            ipady="15px")

        # validate alpha entry
        ttk.Label(self.frame_l, text="数量", font=("华文新魏", 20, "bold")).grid(row=4, column=1, padx='21px',
                                                                                 ipady="15px")
        self.num_entry = ttk.Entry(self.frame_l, validate="focusout", validatecommand=(check_num_func, '%P'))
        self.num_entry.grid(row=4, column=2)
        ttk.Label(self.frame_l, text="(要下载论文\n的数量)", font=("华文行楷", 10, "italic"), style="danger").grid(
            row=4,
            column=3,
            sticky="W",
            padx='21px',
            ipady="15px")

        # validate numeric entry
        ttk.Label(self.frame_l, text="间隔", font=("华文新魏", 20, "bold")).grid(row=5, column=1, padx='21px',
                                                                                 ipady="15px")
        self.div_entry = ttk.Entry(self.frame_l, validate="focusout", validatecommand=(check_div_func, '%P'))
        self.div_entry.grid(row=5, column=2)
        ttk.Label(self.frame_l, text="(太快会被封)", font=("华文行楷", 10, "italic"), style="danger").grid(row=5,
                                                                                                           column=3,
                                                                                                           sticky="W",
                                                                                                           padx='21px',
                                                                                                           ipady="15px")
        # validate numeric entry
        ttk.Label(self.frame_l, text="网站", font=("华文新魏", 20, "bold")).grid(row=6, column=1, padx='21px',
                                                                                 ipady="15px")
        self.look_in = ttk.Combobox(self.frame_l, values=(
            '知网', '美国专利全文数据库', '超星发现', '万方数据知识服务平台', '中国科学引文数据库（CSCD）'))
        self.look_in.grid(row=6, column=2)
        ttk.Label(self.frame_l, text="(目前可用：\n知网)", font=("华文行楷", 10, "italic"), style="danger").grid(row=6,
                                                                                                                column=3,
                                                                                                                sticky="W",
                                                                                                                padx='21px',
                                                                                                                ipady="15px")

        # t = threading.Thread(target=update(), args=())
        # # 守护线程
        # t.setDaemon(True)
        # # 启动线程
        # t.start()
        self.m = ttk.Meter(
            master=self.frame_m,
            metersize=350,
            amountused=0,
            subtext="下载完成度",
            metertype='semi',
            meterthickness=20,
            stripethickness=5,
            textfont="-size 100 -weight bold",
            subtextfont='-size 20',
            textright=" %"
        )
        self.m.grid(row=1)

        self.st = ScrolledText(self.frame_m, height=15, autohide=True)
        self.st.grid(row=3, columnspan=7)
        self.st.insert(INSERT, '运行提示（目前仅支持谷歌浏览器V105）：\n')

        def process(usercode, password, find_data, download_dir, num=20, break_time=0):
            self.m.step(1)
            self.st.insert(INSERT, '开始处理\n')
            self.st.insert(INSERT, '系统兼容出现问题！！\n')
            self.m.step(1)
            files = os.listdir(download_dir)  # 读入文件夹
            self.m.step(1)
            num_old = len(files)
            self.m.step(1)

            # 实例化一个Options
            options = Options()
            self.m.step(1)

            # 用于定义下载不弹窗和默认下载地址（默认下载地址还要再后面的commands里启动，默认是不开启的）
            prefs = {"download.default_directory": download_dir, "download.prompt_for_download": False}
            self.m.step(1)
            options.add_experimental_option("prefs", prefs)
            self.m.step(1)

            # 无头模式（就是不打开浏览器）
            options.add_argument("--headless")
            self.m.step(1)
            options.add_argument("--disable-gpu")
            self.m.step(1)

            driver = webdriver.Chrome(options=options)
            self.m.step(1)

            # print("已启动浏览器")
            self.st.insert(INSERT, '已启动浏览器\n')
            self.m.step(1)
            #
            driver.get(r'http://ras.hbut.edu.cn/reader/user/login')
            self.m.step(1)
            driver.implicitly_wait(30)
            self.m.step(1)
            self.m.step(1)
            self.m.step(1)
            self.m.step(1)
            # print("已打开登陆界面")
            self.st.insert(INSERT, '已打开登陆界面\n')
            self.m.step(1)
            usercode_blank = driver.find_element(By.XPATH, '//*[@id="userCode"]')
            self.m.step(1)
            password_blank = driver.find_element(By.ID, "password")
            self.m.step(1)
            self.st.insert(INSERT, '装载用户名\n')
            self.m.step(1)
            usercode_blank.send_keys(usercode)
            self.m.step(1)
            self.st.insert(INSERT, '装载密码\n')
            self.m.step(1)
            password_blank.send_keys(password)
            self.m.step(1)
            # print("已输入指定登陆信息")
            self.st.insert(INSERT, '已输入指定登陆信息\n')
            self.m.step(1)

            login_button = driver.find_element(By.XPATH, '//*[@id="app"]/div[3]/form/div[3]/button')
            self.m.step(1)
            login_button.click()
            self.m.step(1)
            # print("已登陆")
            self.st.insert(INSERT, '已登陆\n')
            self.m.step(1)

            driver.find_element(By.XPATH, '//*[@id="root"]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div[2]/a').click()
            self.m.step(1)
            driver.close()
            self.m.step(1)
            zw = driver.window_handles
            self.m.step(1)

            driver.switch_to.window(zw[0])
            self.m.step(1)
            driver.implicitly_wait(30)
            self.m.step(1)
            self.st.insert(INSERT, '装载主题\n')
            self.m.step(1)
            driver.find_element(By.XPATH, '//*[@id="txt_SearchText"]').send_keys(find_data)
            self.m.step(1)
            # print("搜索中")
            self.st.insert(INSERT, '搜索中\n')
            self.m.step(1)
            driver.find_element(By.XPATH, '/html/body/div[2]/div[2]/div/div[1]/input[2]').click()  # 知网搜索结果界面
            self.m.step(1)
            # print("已打开知网搜索结果界面")
            self.st.insert(INSERT, '已打开知网搜索结果界面\n')
            self.m.step(1)

            self.st.insert(INSERT, '装载数量\n')
            self.m.step(1)
            pages = (num // 20)
            self.m.step(1)
            each_in_pages = num - pages * 20
            self.m.step(1)
            for page in range(pages + 1):
                if page == range(pages + 1)[-1]:
                    for each_in_page in range(each_in_pages):
                        driver.find_elements(By.CLASS_NAME, 'fz14')[each_in_page].click()  # 点
                        driver.switch_to.window(driver.window_handles[-1])  # 转
                        driver.implicitly_wait(30)
                        driver.find_element(By.XPATH, '//*[@id="pdfDown"]/i').click()  # 下载
                        # print("下载中")
                        self.m.step(int((1 / num) * 60))
                        self.st.insert(INSERT, '下载中\n')
                        driver.implicitly_wait(30)
                        self.st.insert(INSERT, '间隔' + str(break_time) + '\n')
                        sleep(break_time)
                        if len(driver.window_handles) > 2:
                            for item in driver.window_handles[1:len(driver.window_handles) - 1]:
                                driver.switch_to.window(item)
                                driver.close()
                        else:
                            driver.switch_to.window(driver.window_handles[0])
                        driver.switch_to.window(driver.window_handles[0])
                else:
                    for each_in_page in range(20):
                        driver.find_elements(By.CLASS_NAME, 'fz14')[each_in_page].click()  # 点
                        driver.switch_to.window(driver.window_handles[-1])  # 转
                        driver.implicitly_wait(30)
                        driver.find_element(By.XPATH, '//*[@id="pdfDown"]/i').click()  # 下载
                        # print("下载中")
                        self.m.step(int((1 / num) * 60))
                        self.st.insert(INSERT, '下载中\n')
                        driver.implicitly_wait(30)
                        sleep(break_time)
                        if len(driver.window_handles) > 2:
                            for item in driver.window_handles[1:len(driver.window_handles) - 1]:
                                driver.switch_to.window(item)
                                driver.close()
                        else:
                            driver.switch_to.window(driver.window_handles[0])
                        driver.switch_to.window(driver.window_handles[0])

            files = os.listdir(download_dir)  # 读入文件夹
            num_new = len(files)
            print("下载成功：" + str(num_new - num_old) + "下载失败：" + str(num - (num_new - num_old)))
            self.st.insert(INSERT,
                           "下载成功：" + str(num_new - num_old) + "下载失败：" + str(num - (num_new - num_old)) + '\n')
            driver.close()

        def click_start():
            print('启动判断函数\n')
            if self.usercode_flag == 1:
                self.st.insert(INSERT, '启动线程\n')
                download_dir = self.root_dir + r"\下载结果"
                t = threading.Thread(target=process, args=(
                    self.usercode, self.password, self.find_data, download_dir, int(self.num), int(self.break_time)))
                # 守护线程
                t.setDaemon(True)
                # 启动线程
                t.start()
            else:
                self.st.insert(INSERT, '输入不正确\n')

        self.start = ttk.Button(self.frame_m, style="success-outline-toolbutton", text="开始下载！！", width=40,
                                command=click_start)
        self.start.grid(row=2)

        self.photo = ttk.PhotoImage(file=self.root_dir + r"\resource\me.png")
        ttk.Label(self.frame_l, image=self.photo).grid(row=8, column=1, columnspan=3)

        self.beauty_photo = ttk.PhotoImage(file=self.root_dir + r"\resource\beauty3.png")
        beauty = ttk.Label(self.frame_r, image=self.beauty_photo)
        beauty.pack(side="bottom")


def QueryWindow():
    # 显示一个警告信息，点击确后，销毁窗口
    Messagebox.ok(alert=True, message="下次再见啦朋友，番茄龙会记得为你摘一颗星星的", title="saygoodbye")
    root.destroy()


if __name__ == "__main__":
    # create the toplevel window
    root = ttk.Window(title="给阿龙摘星星,使用出现问题请联系作者3157629405", size=[1800, 1000], themename="superhero")
    root.place_window_center()
    root.attributes('-alpha', 0.8)
    root.resizable(False, False)
    root.iconbitmap(default=os.path.abspath(os.curdir) + r'\resource\me.ico')
    root.protocol('WM_DELETE_WINDOW', QueryWindow)
    windon = show_pachong(root)

    root.mainloop()
